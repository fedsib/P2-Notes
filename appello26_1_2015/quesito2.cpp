#include <iostream>
using namespace std;

class Z {
public:
	Z(int x){}
};

class A{
public:
	void f(int){cout << "A::f(int) "; f(true);}
	virtual void f(bool) {cout << "A::f(bool) ";}
	virtual A* f(Z){cout << "A::f(Z) "; f(2); return this;}
	A(){cout << "A() ";}
};

class B: virtual public A{
public:
	void f(const bool&){cout << "B::f(const bool&) ";}
	void f(const int&){cout << "B::f(const int&) ";}
	virtual B* f(Z){cout << "B::f(Z) "; return this;}
	virtual ~B(){cout << "~B ";}
	B() {cout << "B() ";}
};

class C: virtual public A{
public:
	C* f(Z){cout << "C::f(Z) "; return this;}
	C() {cout << "C() ";}
};

class D: public B{
public:
	virtual void f(bool) const {cout << "D::f(bool) ";}
	B* f(Z) {cout << "D::f(Z) "; return this;}
	~D() {cout << "~D ";}
	D() {cout << "D() ";}
};

class E: public D, public C{
public:
	void f(bool){cout << "E::f(bool) ";}
	E* f(Z){cout << "E::f(Z) "; return this;}
	E() {cout << "E() ";}
	~E() {cout << "~E ";}
};

int main() {

	// E *puntE = new E;  				A() B() D() C() E()
	// D *puntD = new D;  				A() B() D()
	// pa3->f(3); 						A::f(int) A::f(bool)
	// pa4->f(3); 						A::f(int) E::f(bool)
	// pb1->f(true); 					B::f(const bool&) 
	// pa4->f(true); 					E::f(bool)
	// pa2->f(Z(2)); 					C::f(Z)
	// pa4->f(Z(2)); 					E::f(Z)
	// pb->f(3); 						B::f(const int&)
	// pc->f(3); 						C::f(Z)
	// (pa4->f(Z(3)))->f(4); 			E::f(Z) A::f(int) E::f(bool)
	// (pc->f(Z(3)))->f(4); 			C::f(Z) C::f(Z)
	// delete pa4; 						NESSUNA STAMPA
	// delete pd; 						B~ D~

}
#include <iostream>


class C {

public:
	C(){}
	C(const C &r){std::cout << "*" << std::endl;}

};


C f(C a){
	
	C b(a);

	C c = b;

	return c;
}

int main(){

	C x; // Dichiara oggetto di tipo C, che viene costruito utilizzando C(){}

	C y = f(f(x));

	/*
		f(x) -> a di tipo C viene costruito di copia
				c di tipo C viene costruito di copia
		f(f(x)) ->	a di tipo C viene costruito di copia
					c di tipo C viene costruito di copia
		C y = f(f(x)) -> y viene costruito di copia

		SOLUZIONE: stampa * * * * *

	 */


}
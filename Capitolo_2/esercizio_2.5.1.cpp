#include <iostream>

class C {

private:
	
	int x;

public:

	C(int n = 0){ x = n;}

	C F(C obj) {C r; r.x = obj.x + x; return r;}
	C G(C obj) const {C r; r.x = obj.x + x; return r;}
	C H(C &obj) {obj.x += x; return obj;}
	C I(const C &obj){C r; r.x = obj.x + x; return r;}
	C J(const C &obj) const {C r; r.x = obj.x + x; return r;}

	int X() const {return x;}
};


int main(){

	C x, y(1), z(2); const C v(2); // Dichiarazione e inizializzazione oggetti

	z = x.F(y); 		// Compila ed esegue correttamente. ** OK **
	// v.F(y);  		// ERRORE! Stiamo invocando un metodo non marcato const su oggetto const ** X **
	v.G(y);				// Compila ed esegue correttamente ** OK **
	(v.G(y)).F(x);		// Compila ed esegue correttamente ** OK **
	(v.G(y)).G(x);		// Compila ed esegue correttamente ** OK **
	// x.H(v);			// ERRORE! v è const ma C parametro formale non è const ** X **
	/* x.H(z.G(y));		   ERRORE! obj è un alias e stiamo passando un oggetto temporaneo anonimo 
						   e quindi non può essere passato come parametro attuale ** X ** 
						*/
	x.I(z.G(y));		// Compila ed esegue correttamente ** OK **
	x.J(z.G(y));		// Compila ed esegue correttamente ** OK **
	v.J(z.G(y));		// Compila ed esegue correttamente ** OK **
						 


	std::cout << "z.X()= " << z.X() << std::endl; 

}
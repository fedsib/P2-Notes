#include <iostream>

/*
	(*) Quando un oggetto viene costruito(invocazione del costruttore) come primissima
		cosa vengono costruiti i campi dati privati e pubblici, poi verrà eseguito
		il corpo del costruttore.

	(*) Se però sono presenti liste di inizializzazione bisogna tenere da conto che 
		verranno eseguite per prime!!!

	(*) È molto importante ricordarsi di inserire SEMPRE un costruttore di default altrimenti
		la compilazione potrebbe terminare con un errore nel caso della costruzione di un 
		oggetto di cui non si è tenuto conto(anche temporanei anonimi).
 */



class C {

private:
	int x;
public:
	C(){std::cout << "C0 "; x = 0;}
	C(int k){std::cout << "C1 "; x = k;}

};

class D {

private:
	C c;
public:
	D(){std::cout << "D0 "; c = C(3);}
};

class E {

private:
	char c;
	C c1;
public:
	D d;
	C c2;
};

int main(){

	D y; std::cout << std::endl;		// STAMPA C0 D0 C1
	E x; std::cout << std::endl;		// STAMPA C0 C0 D0 C1 C0
	E *p = &x; std::cout << std::endl;	// NON STAMPA NULLA
	D &a = y; std::cout << std::endl;	// NON STAMPA NULLA

}
//
// DEFINIRE UN COSTRUTTORE DI DEFAULT LEGALE PER LA CLASSE C
// 


class E {
private:
	int x;
public:
	E(int z = 0) : x(z) {}
};

class C {
private:
	int z;
	E x;
	const E e;
	E &r;
	int *const p;
public:
	C() : z(0), x(), e(), r(x), p(&z){}
};

/*
	- int z; viene costruito con un parametro di default 0
	- E x; viene costruito utilizzando il costruttore di default di E
	- const E e; costruito utilizzando il costruttore di default di E
	- E &r; viene costruito come alias di x che è di tipo E
	- int *const p; viene fatto puntare a z che è di tipo int
	
 */


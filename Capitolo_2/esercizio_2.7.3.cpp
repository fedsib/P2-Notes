#include <iostream>

class Puntatore{

public:

	int *punt;


};


int main(){

	Puntatore x, y;

	x.punt = new int(8); // *punt punta ad un intero nello Heap

	y = x; // Assegnazione -> i campi dati di x vengono copiati in y

	std::cout << "*(y.punt) = " << *(y.punt) << std::endl; // STAMPA *(y.punt) = 8

	*(y.punt) = 3;

	std::cout << "*(x.punt) = " << *(x.punt) << std::endl; // STAMPA *(x.punt) = 3

	delete x.punt;

}
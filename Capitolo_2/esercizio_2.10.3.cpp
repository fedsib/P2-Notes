#include <iostream>
using namespace std;

/*
	Da compilare con -fno-elide-constructors per notare la stampa degli identificatori
	dei costruttori che vengono invocati dai temporanei anonimi!!!

	(*) Vedere capitolo2.txt -> Nota Compilatore
 */



class D {
private:
	int z;
public:
	D(int x=0) : z(x) {cout << "D01 ";}
	D(const D &a) : z(a.z) {cout << "Dc ";}
};

class C {
private:
	D d;
public:
	C() : d(D(5)) {cout << "C0 ";}
	C(int a) : d(5) {cout << "C1 ";}
	C(const C &c) : d(c.d) {cout << "Cc ";}
};


int main(){

	C c1; cout << "UNO" << endl;		// STAMPA D01 C0 UNO
	C c2(3); cout << "DUE" << endl;		// STAMPA D01 C1 DUE
	C c3(c2); cout << "TRE" << endl;	// STAMPA Dc Cc TRE


}
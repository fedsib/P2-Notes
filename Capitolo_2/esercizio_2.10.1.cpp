#include <iostream>

using namespace std;

class D {

public:
	D() { cout << "D0 ";}
	D(int a) { cout << "D1 ";}
};

class E {

private:
	D d;
public:
	E() : d(3) {cout << "E0 ";}
	E(double a, int b) {cout << "E2 ";}
	E(const E &a) : d(a.d) {cout << "Ec  ";}
};

class C {
private:
	int z;
	E e;
	D d;
	E *p;
public:
	C() : p(0), e(), z(4) {cout << "C0 ";}
	C(int a, E b) : e(3.7,2), p(&b), z(1), d(3) {cout << "C1 ";}
	C(char a, int b) : e(), d(2), p(&e) {cout << "C2 ";}
};

int main(){

	E e; cout << endl; 			// STAMPA D1 E0
	C c; cout << endl;			// STAMPA D0 E1 D0 C0
	C c1(1,e); cout << endl;	// STAMPA Ec E2 D1 C1 (*)
	C c2('b',2); cout << endl;	// STAMPA D1 E0 D1 C2

	/*
		(*) Da notare che viene invocato il costruttore di copia di E che a sua volta
			vorrebbe invocare il costruttore di copia di D che però è quello standard
			quindi non viene stampato nulla.
	 */


}
#include <iostream>

using namespace std;


class A {
public:
	A(int x = 0){cout << x << "A()" << endl;}
};

template<class T>
class C {
public:
	static A s;
	
};

template<class T>
A C<T>::s = A();


int main(){

	C<double> c;
	C<int> d;
	C<int>::s = A(2);

	/*
		STAMPA
		0A() 2A() // Per prima cosa viene costruito il campo s di C quando viene istanziato
				  // l'oggetto c poi viene invocato il costruttore nella terza riga di codice
				  // quindi stampa il parametro attuale 2 e A()
	 */
}
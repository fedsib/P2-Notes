#include <iostream>
using namespace std;

template<int I>
class C {
static int numero;
public:
	C();
	void stampa();
};

template<int I>
int C<I>::numero = I;

template<int I>
C<I>::C() {numero++;}

template<int I>
void C<I>::stampa(){
	cout << "Valore statico: " << numero << endl;
}

int main(){

	C<1> uno;
	C<1> uno_b;
	C<2> due_a;
	C<2> due_b;
	
	uno.stampa();			// Valore statico: 3
	uno_b.stampa();			// Valore statico: 3
	
	due_a.stampa();			// Valore statico: 4
	due_a.stampa();			// Valore statico: 4
}
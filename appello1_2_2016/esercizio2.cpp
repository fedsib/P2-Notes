#include <list>
#include <vector>
#include <iostream>
using namespace std;

class B {
private:
	list<double> *ptr;
	virtual void m() = 0;
};

class D: virtual public B {
private:
	int x;

};

class C: virtual public B{};

class E: public C, public D{
private:
	vector<int*> v;
public:
	void m(){}
	E(){}
	E(const E &e) : B(e), C(e), D(e), v(e.v){}
};
#include <iostream>
using namespace std;

class C {
public:
	int x;
	void f(){x=1;}
};

class D :public C {
public:
	int y;
	void f(){C::f(); y=2;}
};

int main(){

	C c;
	D d;
	c.f();
	d.f();

	cout << c.x << endl;
	cout << d.x << " " << d.y << endl;

	/*
		STAMPA
		1
		1 2

		c.f() => invoca la funzione f() definita nella classe C
		d.f() => invoca la funzione f() definita nella classe
				 derivata D che a sua volta invoca la f() di C

	 */

}
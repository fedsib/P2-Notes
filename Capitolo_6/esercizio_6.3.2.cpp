#include <iostream>
using namespace std;

class C{
private:
	int i;
protected:
	int p;
public:
	C() {cout << "C0 ";}
	C(int x){cout << "C1 ";}
};

class D :private C {
protected:
	int j;
public:
	D() : C(2){cout << "D0 ";}
	D(double x){cout << "D1 ";}
};

class E :public C{
private:
	D d;
public:
	int k;
	E() : C(6) {cout << "E0 ";}
};

class F :public D{
protected:
	E e;
public:
	F() : D(3.2){cout << "F0 ";}
	F(float x) {cout << "F1 ";}
};

class G :public E {
private:
	F f;
	C c;
public:
	G() : E() {cout << "G0 ";}
	G(char x){cout << "G1 ";}
};

int main(){G g;}


/*
	STAMPA
	C1 C1 D0 E0 C0 D1 C1 C1 D0 E0 F0 C0 G0

	Invocazioni:

		1)  G()		|
		2)  E()		|
		3)  C(6)    |=> C1 C1 D0 E0 C0
		4)  D()		|
		5)  C(2)	|

		6)  F()		|
		7)  D(3.2)	|
		8)  C()		|=> D1 C1 C1 D0 E0 F0
		9)  E()		|
		10) C(6)	|

		11) C()		|=> C0

		12) Fine della costruzione di G g; viene eseguito il corpo del costruttore => G0

 */

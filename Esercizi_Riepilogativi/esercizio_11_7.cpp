
#include <iostream>
using namespace std;


class A {
public:
	virtual ~A(){}
};

class B: public A{};

class C: virtual public B{};

class D: virtual public B{};

class E: public C, public D{};


/**
 * 			Gerarchia
 *			
 *				A
 *				|
 *				|
 *			  __B__
 *			 |     |
 *    		 |     |
 *       	 C__ __D
 *         		|
 *           	|
 *           	E
 * 
 */



char F(A* p, C& r){

	B *punt = dynamic_cast<B*>(p);

	try{
		E& s = dynamic_cast<E&>(r);
	}

	catch(bad_cast){
		if(punt) return 'O';
		else return 'M'
	}
	if (punt) return 'R';
	return 'A';
}

int main(){
	A a; B b; C c; D d; E e;
	cout << F(&b,e) << F(&b,c) << F(&a,c) << F(&a,e) << endl;
}
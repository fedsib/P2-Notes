#ifndef DATA_H
#define DATA_H

#include <string>
#include <iostream>


class Data{
private:
	std::string giornoSett;
	int giorno;
	int mese;
	int anno;

	void cambiaGiornoS();


public:
	Data(std::string ="lun", int =1, int =1, int =2000);
	Data(const Data &d);
	std::string GiornoS() const;
	int Giorno() const;
	int Mese() const;
	int Anno() const;

	bool operator==(const Data&) const;
	bool operator<(const Data&) const;

	void avanzaUnGiorno();

};

std::ostream& operator<<(std::ostream&,const Data&); 





#endif


#include "data.h"

Data::Data(std::string giornoS, int g, int m, int a) : giornoSett(giornoS),
													   giorno(g),
													   mese(m),
													   anno(a){}

Data::Data(const Data &d) : giornoSett(d.GiornoS()), 
							giorno(d.Giorno()),
							mese(d.Mese()),
							anno(d.Anno()){}

std::string Data::GiornoS() const { return giornoSett;}

int Data::Giorno() const { return giorno;}

int Data::Mese() const { return mese;}

int Data::Anno() const { return anno;}

bool Data::operator==(const Data &d) const {

	return ((giornoSett == d.GiornoS()) && (giorno == d.Giorno()) && (mese == d.Mese()) && (anno == d.Anno()));
}

bool Data::operator<(const Data &d) const {

	if (anno < d.Anno())
		return true;

	if (anno == d.Anno() && mese < d.Mese())
		return true;

	if (anno == d.Anno() && mese == d.Mese() && giorno < d.Giorno())
		return true;

	return false;
}

void Data::avanzaUnGiorno() {

	if (giorno == 31){

			giorno = 1;
			mese++;

			if (mese == 12){

				mese = 1;
				anno++;

			}

	} else giorno++;

	cambiaGiornoS();

}

void Data::cambiaGiornoS() {


	if (giornoSett == "lun")
		giornoSett = "mar";
	else
		if (giornoSett == "mar")
			giornoSett = "mer";
		else
			if (giornoSett == "mer")
				giornoSett = "gio";
			else
				if (giornoSett == "gio")
					giornoSett = "ven";
				else
					if (giornoSett == "ven")
						giornoSett = "sab";
					else
						if (giornoSett == "sab")
							giornoSett = "dom";
						else 
							if (giornoSett == "dom")
								giornoSett = "lun";


}


std::ostream& operator<<(std::ostream &os, const Data &d){

	return os << d.GiornoS() << " - " << d.Giorno() << "/" << d.Mese() << "/" << d.Anno();
}


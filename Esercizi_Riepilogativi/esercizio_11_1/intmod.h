#ifndef INTMOD_H
#define INTMOD_H


/*
	Definire una classe IntMod i cui oggetti rappresentano numeri interi
	modulo un dato intero (ex: 2 mod 3). Devono essere disponibili gli operatori
	di somma e prodotto tra oggetti IntMod e convertitori di tipo affinché gli oggetti
	IntMod siano utilizzabili come interi.
	
	(!) Quando in un'espressione compaiono sia espressioni intere che oggetti IntMod il 
		tipo di espressione deve essere di tipo intero.
 */



class IntMod{
private:
	int x;
	static int modulo;
public:
	explicit IntMod(int =0);
	operator int();
	static void setModulo(int m);
	IntMod operator+(const IntMod&) const;
	IntMod operator*(const IntMod&) const;
};

#endif
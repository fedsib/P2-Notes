#include "intmod.h"

int IntMod::modulo = 1;


IntMod::IntMod (int _x) : x(_x%modulo){} 

void IntMod::setModulo(int m) {

	modulo = m;

}

IntMod::operator int() {

	return x;
}


IntMod IntMod::operator+(const IntMod &y) const{

	return IntMod(x+y.x);

}

IntMod IntMod::operator*(const IntMod &y) const{

	return IntMod(x*y.x);
}
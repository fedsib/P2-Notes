#include "intmod.h"
#include <iostream>


int main() {

	IntMod::setModulo(2);

	IntMod n1(3), n2(4);

	std::cout << n1+n2 << std::endl; 
	std::cout << n1*n2 << std::endl;

	IntMod::setModulo(3);

	IntMod n3(5), n4(8);

	std::cout << n3*n4 << std::endl;
}
#include <iostream>
using namespace std;

class A {
public:
	virtual ~A(){}
};

class B: virtual public A {};

class C: virtual public A {};

class D: public B, public C {};

class E: public D {};

class F: public E {};

template<class T>
void Fun(T &ref) {

	bool b = 0;

	B *p = &ref;

	try{ throw ref;}

	catch(E){cout << "E "; b=1;}
	catch(D){cout << "D "; b=1;}
	catch(B){cout << "B "; b=1;}
	catch(A){cout << "A "; b=1;}
	catch(C){cout << "C "; b=1;}

	if (b==0) cout << "ZERO ";
}

int main(){
	
	A a; B b; C c; D d; E e; F f;
	A *pa = &b; D *pd = &f;
	B *pb = dynamic_cast<B*>(pa);
	C *pc = dynamic_cast<E*>(pd);
	E *pe = dynamic_cast<E*>(pd);
	

	// Fun(a); 			// NON COMPILA: non posso inizializzare un B* con un A*, errore di tipo
	// Fun(b);    		// STAMPA: B
	// Fun(c); 			// NON COMPILA: non posso inizializzare un B* con un C*, errore di tipo
	// Fun(d);			// STAMPA: D
	// Fun(e);			// STAMPA: E
	// Fun(f);			// STAMPA: E (non c'è il catch col tipo F, quindi entra nel catch col padre più vicino)
	// Fun(*pa);		// NON COMPILA: il tipo statico è A* ma il puntatore p in Fun è B*, errore di tipo
	// Fun(*pb);		// STAMPA: B
	// Fun(*pc);		// NON COMPILA: il tipo statico è C* ma il puntatore p in Fun è B*, errore di tipo
	// Fun(*pd);		// STAMPA: D
	// Fun(*pe);		// STAMPA: E
	// Fun<B>(*pd);		// STAMPA: B
	// Fun<D>(*pd);		// STAMPA: D
	// Fun<E>(*pd);		// NON COMPILA: binding tra E e D fallisce poiché D non è sottotipo di E ma suo supertipo
}










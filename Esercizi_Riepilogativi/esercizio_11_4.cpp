#include <string>
using namespace std;

/*
	ESERCIZIO

	Ridefinire il costruttore di copia di Albero in modo che esegua copie profonde
 */



class Nodo{
private:
	Nodo(string st="***", Nodo *s=0, Nodo *d=0) : info(st), sx(s), dx(d){}
	string info;
	Nodo *sx;
	Nodo *dx;
};

class Albero{
	friend class Nodo;
public:
	Albero() : radice(0){}
	Albero(const Albero &t) : radice(copia(t.radice)); // COSTRUTTORE DI COPIA
private:
	Nodo *radice;
	Nodo* copia(Nodo *r);
}

// IMPLEMENTAZIONE
Nodo* Albero::copia(Nodo *r){

	if(r == 0)
		return 0;

	Nodo *t = new Nodo();

	t->info = r->info;

	t->sx = copia(r->sx);
	t->dx = copia(r->dx);

	return t;

}
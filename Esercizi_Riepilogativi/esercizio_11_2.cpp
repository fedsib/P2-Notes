#include <iostream>
#include <string>
using namespace std;


class C {
private:
	int d;
public:
	C(string s="") : d(s.size()){}
	explicit C(int n) : d(n){}
	operator int() {return d;} // Agisce da convertitore esplicito C->int
	C operator+(C x) {return C(d+x.d);}
};

int main(){

	C a, b("pippo"), c(3);

	cout << a << ' ' << 1+b << ' ' << c+4 << ' ' << c+b << endl;

	/*
		STAMPA

		0 6 7 8

	 */


}
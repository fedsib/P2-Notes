#ifndef BENZINA_H
#define BENZINA_H

#include "auto.h"

class Benzina: public Auto {
private:
	static int bonus;
	bool euro4;
public:
	Benzina(int,bool =false);
	double tassa() const;


};


#endif
#include "benzina.h"

int Benzina::bonus = 50;

Benzina::Benzina(int x, bool b) : Auto(x), euro4(b){}

double Benzina::tassa() const {

	double calc = cavalliFiscali()*tassaPerCavallo();

	if (euro4)
		return calc - bonus;

	return calc;

}
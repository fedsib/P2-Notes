#ifndef ACI_H
#define ACI_H

#include <vector>
#include "auto.h"

class ACI{

private:
	std::vector<Auto*> veicoli;
public:
	void aggiungiAuto(const Auto&);
	double incassaBolli() const;

};


#endif
#ifndef DIESEL_H
#define DIESEL_H


#include "auto.h"

class Diesel: public Auto {

private:
	static int add_fiscale;

public:
	Diesel(int);
	double tassa() const;



};



#endif
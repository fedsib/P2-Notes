#include "diesel.h"

int Diesel::add_fiscale = 100;

Diesel::Diesel(int x) : Auto(x){}

double Diesel::tassa() const {

	return (cavalliFiscali()*tassaPerCavallo()) + add_fiscale;
}
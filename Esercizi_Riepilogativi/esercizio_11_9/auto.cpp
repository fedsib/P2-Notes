#include "auto.h"

double Auto::tassa_per_cavallo = 5;

Auto::Auto(int x) : n_cavalli_fiscali(x){}

Auto::~Auto() {}

int Auto::cavalliFiscali() const {
	return n_cavalli_fiscali;
}

double Auto::tassaPerCavallo() {
	return tassa_per_cavallo;
}
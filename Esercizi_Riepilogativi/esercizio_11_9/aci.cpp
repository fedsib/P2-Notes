#include "aci.h"



void ACI::aggiungiAuto(const Auto &a){

	Auto &c = const_cast<Auto&>(a);

	veicoli.push_back(&c);

}

double ACI::incassaBolli() const {

	double tot = 0.0;

	for (std::vector<Auto*>::const_iterator it = veicoli.begin(); it != veicoli.end(); ++it){

		tot += (*it)->tassa();

	}

	return tot;
}
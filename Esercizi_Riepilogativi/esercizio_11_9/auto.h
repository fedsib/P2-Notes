#ifndef AUTO_H
#define AUTO_H

class Auto {
private:
	int n_cavalli_fiscali;
	static double tassa_per_cavallo;
protected:
	Auto(int =0);
	virtual ~Auto();
public:
	int cavalliFiscali() const;
	static double tassaPerCavallo();
	virtual double tassa() const = 0;

};

#endif
#include "diesel.h"
#include "benzina.h"
#include "aci.h"
#include <iostream>

int main(){

	ACI aci;

	Diesel bmw(90), audi(80), wolkswagen(80);
	Benzina lancia(60,true), fiat(60,false);

	aci.aggiungiAuto(bmw);
	aci.aggiungiAuto(audi);
	aci.aggiungiAuto(wolkswagen);
	aci.aggiungiAuto(lancia);
	aci.aggiungiAuto(fiat);

	std::cout << "Totale incassato: " << aci.incassaBolli() << std::endl;


}
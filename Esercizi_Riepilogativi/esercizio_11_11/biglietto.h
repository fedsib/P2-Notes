#ifndef BIGLIETTO_H
#define BIGLIETTO_H


#include <string>

class Biglietto {
private:
	std::string acquirente;
	bool galleria;
protected:
	Biglietto(std::string,bool =false);
	virtual ~Biglietto();
public:
	bool isGalleria() const;
	std::string Nome() const;
};


#endif
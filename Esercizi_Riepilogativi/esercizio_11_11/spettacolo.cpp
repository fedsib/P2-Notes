#include "spettacolo.h"
#include "postonumerato.h"
#include "postononnumerato.h"

Spettacolo::Spettacolo(double pA,double pB,int pNMax, int maxF, int nV) : prezzoA(pA),
																		  prezzoB(pB),
																		  postiNumMax(pNMax),
																		  maxFila(maxF),
																		  postiNumVenduti(nV){}


void Spettacolo::aggiungiBiglietto(const Biglietto &b){

	Biglietto *p = const_cast<Biglietto*>(&b);

	if (dynamic_cast<PostoNonNumerato*>(p))
		biglietti.push_back(p);
	else 
		if (dynamic_cast<PostoNumerato*>(p)){

			if (postiNumVenduti < postiNumMax){
				postiNumVenduti++;
				biglietti.push_back(p);
			}

		}
}

double Spettacolo::prezzo(const Biglietto &b) const {

	Biglietto *p = const_cast<Biglietto*>(&b);
	PostoNumerato *pN = dynamic_cast<PostoNumerato*>(p);
	double tot = 0;

	if (pN){
		if (pN->Fila() <= maxFila)
			return 2*prezzoA+2*prezzoB;
		return 2*prezzoA;
	} else {
		PostoNonNumerato *pNN = dynamic_cast<PostoNonNumerato*>(p);

		if(pNN->isGalleria())
			tot = prezzoB;
		else
			tot = prezzoA+prezzoB;

		if (pNN->Ridotto())
			tot -= prezzoA/2;
	}

	return tot;
}

double Spettacolo::incasso() const {

	double tot = 0;

	for (std::list<Biglietto*>::const_iterator it = biglietti.begin(); it != biglietti.end(); ++it){


		tot += prezzo(**it);

	}

	return tot;

}
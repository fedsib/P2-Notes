#include "postonumerato.h"
#include "postononnumerato.h"
#include "spettacolo.h"
#include <iostream>


int main() {

	Spettacolo s(10,6,200,10);

	PostoNumerato a1("pippo",2);
	PostoNumerato a2("pluto",17);
	PostoNonNumerato b1("topolino");
	PostoNonNumerato b2("paperoga",false,true);

	s.aggiungiBiglietto(a1);
	s.aggiungiBiglietto(a2);
	s.aggiungiBiglietto(b1);
	s.aggiungiBiglietto(b2);

	std::cout << s.incasso() << std::endl;



}
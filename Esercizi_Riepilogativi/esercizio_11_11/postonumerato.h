#ifndef POSTO_NUMERATO_H
#define POSTO_NUMERATO_H

#include "biglietto.h"


class PostoNumerato: public Biglietto {
private:
	int fila;
public:
	PostoNumerato(std::string, int);
	int Fila() const;
};


#endif
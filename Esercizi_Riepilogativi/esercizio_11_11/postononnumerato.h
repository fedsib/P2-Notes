#ifndef POSTO_NON_NUMERATO_H
#define POSTO_NON_NUMERATO_H

#include "biglietto.h"

class PostoNonNumerato: public Biglietto {
private:
	bool ridotto;
public:
	PostoNonNumerato(std::string,bool =true,bool =false);
	bool Ridotto() const;
};


#endif
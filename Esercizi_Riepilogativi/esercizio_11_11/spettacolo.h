#ifndef SPETTACOLO_H
#define SPETTACOLO_H

#include "biglietto.h"
#include <list>

class Spettacolo {
private:
	double prezzoA, prezzoB;
	int postiNumMax, maxFila, postiNumVenduti;
	std::list<Biglietto*> biglietti;
public:
	Spettacolo(double,double,int,int,int =0);
	void aggiungiBiglietto(const Biglietto&);
	double prezzo(const Biglietto&) const;
	double incasso() const;

};


#endif
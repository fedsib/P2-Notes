#include <iostream>
using namespace std;


class A {
private:
	int z;
public:
	~A() {cout <<"Ad ";}
};

class B {
public:
	A *p;
	A a;
	~B() {cout << "Bd ";}
};

class C {
public:
	static B s;
	int k;
	A a;
	~C() {cout << "Cd ";}
};

B C::s = B();


int main(){

	cout << "\n";

	C c1, c2;

	/*
		STAMPA
		Bd Ad ->nella creazione del campo C::s viene invocato il distruttore di B perché viene
				distrutto un temporaneo anonimo assegnato a C::s
		Cd Ad Cd Ad Bd Ad ->distruzione di c2: distrutto il campo di tipo A. 
						  ->distruzione di c1: distrutto il campo di tipo A
						  ->distruzione B statico della classe C

	 */

}

#include <iostream>
using namespace std;


class C {
public:
	string s;
	C(string x="1") : s(x) {}
	~C() {cout << s << "Cd ";}
};

C F(C p){ return p;}

C w("3");

class D {
public:
	static C c;
};

C D::c("4");


int main(){

	cout << "PROGRAMMA UNO\n";

	C x("5"), y("6"); D d;

	y = F(x); cout << "uno\n"; 
	C z = F(x); cout << "due\n";

	/*
		STAMPA

		PROGRAMMA UNO
		5Cd 5Cd uno ->distruzione del parametro formale + distruzione temporaneo ritornato
		5Cd 5Cd due ->distruzione parametro formale + distruzione temporaneo ritornato
		5Cd 5Cd 5Cd 4Cd 3Cd ->distruzione z,y,x,d,w
	 */
}

#include <iostream>
using namespace std;


class C {
public:
	string s;
	C(string x="1") : s(x) {}
	~C() {cout << s << "Cd ";}
};

C& F(C& p){ return p;}

C w("3");

class D {
public:
	static C c;
};

C D::c("4");


int main(){

	cout << "PROGRAMMA TRE\n";

	C x("5"), y("6"); D d;

	y = F(x); cout << "uno\n";
	C z=F(x); cout << "due\n";

	/*
		STAMPA
		PROGRAMMA TRE
		uno ->nessuna distruzione (notare il passaggio di parametri e il tipo di ritorno di F)
		due ->nessuna distruzione (come sopra)
		5Cd 5Cd 5Cd 4Cd 3Cd
	 */

}
